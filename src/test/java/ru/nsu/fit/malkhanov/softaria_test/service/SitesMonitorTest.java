package ru.nsu.fit.malkhanov.softaria_test.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import ru.nsu.fit.malkhanov.softaria_test.model.MonitorInfo;

import java.util.HashMap;
import java.util.Set;

class SitesMonitorTest {
    private final HashMap<String, String> yesterday = new HashMap<>();
    private final HashMap<String, String> today = new HashMap<>();

    @BeforeEach
    void setUp() {
        today.put("1", "aa");
        today.put("2", "b");
        today.put("4", "c");
        yesterday.put("1", "a");
        yesterday.put("2", "b");
        yesterday.put("3", "c");
    }

    @Test
    void getStatisticsUsingGuava() {
        SitesMonitor monitor = new SitesMonitor(yesterday, today);
        MonitorInfo result = monitor.getStatisticsUsingGuava();
        Assertions.assertEquals(Set.of("3"), result.getRemoved());
        Assertions.assertEquals(Set.of("4"), result.getAdded());
        Assertions.assertEquals(Set.of("1"), result.getModified());
    }

    @Test
    void getStatistics() {
        SitesMonitor monitor = new SitesMonitor(yesterday, today);
        MonitorInfo result = monitor.getStatistics();
        Assertions.assertEquals(Set.of("3"), result.getRemoved());
        Assertions.assertEquals(Set.of("4"), result.getAdded());
        Assertions.assertEquals(Set.of("1"), result.getModified());
    }
}