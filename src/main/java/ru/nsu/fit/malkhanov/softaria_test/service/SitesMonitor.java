package ru.nsu.fit.malkhanov.softaria_test.service;

import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.fit.malkhanov.softaria_test.model.MonitorInfo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class SitesMonitor {
    private final HashMap<String, String> yesterday;
    private final HashMap<String, String> today;

    public MonitorInfo getStatisticsUsingGuava() {
        MonitorInfo result = new MonitorInfo();
        MapDifference<String, String> diff = Maps.difference(yesterday, today);
        result.setModified(diff.entriesDiffering().keySet());
        result.setAdded(diff.entriesOnlyOnRight().keySet());
        result.setRemoved(diff.entriesOnlyOnLeft().keySet());
        return result;
    }

    public MonitorInfo getStatistics() {
        MonitorInfo result = new MonitorInfo();
        Set<String> removed = new HashSet<>(yesterday.keySet());
        Set<String> added = new HashSet<>();
        Set<String> modified = new HashSet<>();

        today.forEach((String url, String page) -> {
            String other = yesterday.get(url);
            if (other == null) {
                added.add(url);
            } else {
                removed.remove(url);
                if (!page.equals(other)) {
                    modified.add(url);
                }
            }
        });

        result.setAdded(added);
        result.setModified(modified);
        result.setRemoved(removed);
        return result;
    }
}
