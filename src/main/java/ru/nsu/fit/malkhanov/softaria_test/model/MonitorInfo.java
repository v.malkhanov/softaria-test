package ru.nsu.fit.malkhanov.softaria_test.model;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class MonitorInfo {
    private Set<String> removed;
    private Set<String> added;
    private Set<String> modified;

    public String getRemoved(String delimiter) {
        return String.join(delimiter, removed);
    }

    public String getAdded(String delimiter) {
        return String.join(delimiter, added);
    }

    public String getModified(String delimiter) {
        return String.join(delimiter, modified);
    }
}
