package ru.nsu.fit.malkhanov.softaria_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class SoftariaTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(SoftariaTestApplication.class, args);
    }
}
