package ru.nsu.fit.malkhanov.softaria_test.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.nsu.fit.malkhanov.softaria_test.model.MonitorInfo;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MailService {
    private static final String TEMPLATE_MESSAGE;

    private final JavaMailSender mailSender;
    private final SitesMonitor monitor;

    @Value("${spring.mail.username}")
    private String username;

    @Scheduled(cron = "0 0 9 * * ?")
    public void sendStatistics() {
        MonitorInfo statistics = monitor.getStatisticsUsingGuava();
        String text = String.format(TEMPLATE_MESSAGE, statistics.getRemoved(", "),
                statistics.getAdded(", "), statistics.getModified(", "));
        this.sendSimpleMessage("{destination}", "Мониторинг сайтов", text);
    }

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(username);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        mailSender.send(message);
    }

    static {
        TEMPLATE_MESSAGE = "Здравствуйте, дорогая и.о. секретаря\n" +
                "За последние сутки во вверенных Вам сайтах произошли следующие изменения:\n\n" +
                "Исчезли следующие страницы:\n%s\n\n" +
                "Появились следующие новые страницы:\n%s\n\n" +
                "Изменились следующие страницы:\n%s\n\n" +
                "\nС уважением,\n" +
                "автоматизированная система мониторинга\n";
    }
}